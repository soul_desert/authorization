package com.my.app;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.*;

public class App
{

    public static void main(String[] args )
    {
        Scanner in = new Scanner(System.in);
        System.out.println( "Welcome!" );
        while (true) {
            System.out.println( "Press '1' to log in, '2' to create new user or '3' to exit" );
            System.out.print( "-> " );
            int choice;

            while (true) {
                try {
                    choice = in.nextInt();
                    break;
                } catch (InputMismatchException e) {
                    System.out.println("Invalid value! Try again...");
                    System.out.print( "-> " );
                    in = new Scanner(System.in);
                }
            }

            switch (choice) {
                case 1:
                    LogIn(); /* логин */
                    break;
                case 2:
                    Registrate(); /* регистрация */
                    break;
                case 3:
                    System.exit(0);
                default:
                    System.out.println("Invalid value! Try again...");
                    break;
            }

        }

    }

    private static void LogIn() {
        System.out.println("User name: ");
        Scanner in = new Scanner(System.in);
        String probableUser = in.nextLine();
        //Map<String, String> data = new HashMap<>();

        try(FileReader reader = new FileReader("database.txt"))
        {
            BufferedReader buffer = new BufferedReader(reader);
            while(true) {
                String record = buffer.readLine();
                if (record == null) {
                    System.out.println("No such user!");
                    break;
                }
                String[] recordElements = record.split("→");
                if (probableUser.equals(recordElements[0])) {
                    System.out.println("Password: ");
                    if (!DigestUtils.md5Hex(in.nextLine() + recordElements[1]).equals(recordElements[2])) {
                        System.out.println("Incorrect password!");
                    } else System.out.println("Access granted!");
                    break;
                }
            }

        }
        catch(IOException e){
            e.printStackTrace();
        }

        System.out.println("Type 3 to exit or everything you want to continue");
        if (in.nextLine().equals("3")) System.exit(0);

    }

    private static void Registrate() {
        Scanner in = new Scanner(System.in);
        System.out.println("User name: ");
        String username = in.nextLine();
        System.out.println("Password: ");
        String password = in.nextLine();
        Date curTime = new Date();
        String salt = DateFormat.getDateInstance().format(curTime);
        String md5Hex = DigestUtils.md5Hex(password + salt);

        try(FileWriter writer = new FileWriter("database.txt", true))
        {
            writer.write(username + "→" + salt + "→" + md5Hex + "\n");          // "→" - разделитель
            writer.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("New user created!");
        System.out.println("Type 3 to exit or everything you want to continue");
        if (in.nextLine().equals("3")) System.exit(0);
    }
}